local E, L, V, P, G, _ = unpack(ElvUI);
local IFO = E:NewModule('InspectFrameOptions', 'AceEvent-3.0', 'AceTimer-3.0');
local LSM = LibStub("LibSharedMedia-3.0")

local f = CreateFrame('Frame', 'KahootzArmory')
local C = SLArmoryConstants


f:RegisterEvent("ADDON_LOADED") -- Blizz_InspectUI doesn't load until it's needed. Can't secure hook functions until it does.
f:RegisterEvent("INSPECT_READY")

f:SetScript("OnEvent", function(this, event, ...) return IFO[event](self, ...) end)
function IFO:ADDON_LOADED(name)
	if (name == "Blizzard_InspectUI") then
		IFO:AfterLoad()
	end
end

local function GemSocket_OnClick(self, button)
	self = self:GetParent()

	if CursorHasItem() then
		local CursorType, _, ItemLink = GetCursorInfo()

		-- Check cursor item is gem type
		if CursorType == 'item' and select(6, GetItemInfo(ItemLink)) == select(8, GetAuctionItemClasses()) then
			SocketInventoryItem(GetInventorySlotInfo(self.slotName))
			ClickSocketButton(self.socketNumber)

			return
		end
	end

	if self.GemItemID then
		local itemName, itemLink = GetItemInfo(self.GemItemID)

		if not IsShiftKeyDown() then
			SetItemRef(itemLink, itemLink, 'LeftButton')
		else
			if button == 'RightButton' then
				SocketInventoryItem(GetInventorySlotInfo(self.slotName))
			elseif HandleModifiedItemClick(itemLink) then
			elseif BrowseName and BrowseName:IsVisible() then
				AuctionFrameBrowse_Reset(BrowseResetButton)
				BrowseName:SetText(itemName)
				BrowseName:SetFocus()
			end
		end
	end
end

local function GemSocket_OnRecieveDrag(self)
	self = self:GetParent()

	if CursorHasItem() then
		local CursorType, _, ItemLink = GetCursorInfo()

		if CursorType == 'item' and select(6, GetItemInfo(ItemLink)) == select(8, GetAuctionItemClasses()) then
			SocketInventoryItem(GetInventorySlotInfo(self.slotName))
			ClickSocketButton(self.socketNumber)
		end
	end
end

function IFO:GetItemLvL(unit)
	local total, item = 0, 0

	for i = 1, #C.GearList do
		local slot = GetInventoryItemLink(unit, GetInventorySlotInfo((C.GearList[i])))
		if (slot ~= nil) then
			local _, _, _, ilvl = GetItemInfo(slot)
			local upgrade = slot:match(":(%d+)\124h%[")
			if ilvl ~= nil then
				item = item + 1
				total = total + ilvl + (upgrade and C.ItemUpgrade[upgrade] or 0)
			end
		end
	end

	if (total < 1 or item < 15) then
		return
	end
	return floor(total / item);
end

local function CreateArmoryFrame(self)
	--<< Core >>--
	self:Point('TOPLEFT', InspectFrameInset, 10, 20)
	self:Point('BOTTOMRIGHT', InspectFrameInsetRight, 'BOTTOMLEFT', -10, 5)

	--<< Background >>--
	self.BG = self:CreateTexture(nil, 'OVERLAY')
	self.BG:SetInside()

	--<< Change Model Frame's frameLevel >>--
	InspectModelFrame:SetFrameLevel(self:GetFrameLevel() + 2)

	--<< Average Item Level >>--
	C.Toolkit.TextSetting(self, nil, { ['Tag'] = 'AverageItemLevel', ['FontSize'] = 12, }, 'BOTTOM', InspectModelFrame, 'TOP', 0, 14)
	local function ValueColorUpdate()
		self.AverageItemLevel:SetText(C.Toolkit.Color_Value(L['Average'])..' : '..format('%.2f', IFO:GetItemLvL(InspectFrame.unit)))
	end
	E.valueColorUpdateFuncs[ValueColorUpdate] = true

	-- Create each equipment slots gradation, text, gem socket icon.
	local Slot
	for i, slotName in pairs(C.GearList) do
		-- Equipment Tag
		Slot = CreateFrame('Frame', nil, self)
		Slot:Size(130, 41)
		Slot:SetFrameLevel(InspectModelFrame:GetFrameLevel() + 1)
		Slot.Direction = i%2 == 1 and 'LEFT' or 'RIGHT'
		Slot.ID, Slot.EmptyTexture = GetInventorySlotInfo(slotName)
		Slot:Point(Slot.Direction, _G['Inspect'..slotName], Slot.Direction == 'LEFT' and -1 or 1, 0)

		-- Grow each equipment slot's frame level
		_G['Inspect'..slotName]:SetFrameLevel(Slot:GetFrameLevel() + 2)

		-- Gradation
		Slot.Gradation = Slot:CreateTexture(nil, 'OVERLAY')
		Slot.Gradation:SetInside()
		Slot.Gradation:SetTexture('Interface\\AddOns\\ElvUI_SLE\\media\\textures\\Gradation.tga')

		if Slot.Direction == 'LEFT' then
			Slot.Gradation:SetTexCoord(0, .5, 0, .5)
		else
			Slot.Gradation:SetTexCoord(.5, 1, 0, .5)
		end
	
		if slotName ~= 'ShirtSlot' and slotName ~= 'TabardSlot' then
			-- Item Level
			C.Toolkit.TextSetting(Slot, nil, { ['Tag'] = 'ItemLevel', ['FontSize'] = 10, ['directionH'] = Slot.Direction, }, 'TOP'..Slot.Direction, _G['Inspect'..slotName], 'TOP'..(Slot.Direction == 'LEFT' and 'RIGHT' or 'LEFT'), Slot.Direction == 'LEFT' and 2 or -2, -1)

			-- Enchantment Name
			C.Toolkit.TextSetting(Slot, nil, { ['Tag'] = 'ItemEnchant', ['FontSize'] = 8, ['directionH'] = Slot.Direction, }, Slot.Direction, _G['Inspect'..slotName], Slot.Direction == 'LEFT' and 'RIGHT' or 'LEFT', Slot.Direction == 'LEFT' and 2 or -2, 1)
			Slot.EnchantWarning = CreateFrame('Button', nil, Slot)
			Slot.EnchantWarning:Size(E.db.sle.characterframeoptions.itemenchant.warningSize)
			Slot.EnchantWarning.Texture = Slot.EnchantWarning:CreateTexture(nil, 'OVERLAY')
			Slot.EnchantWarning.Texture:SetInside()
			Slot.EnchantWarning.Texture:SetTexture('Interface\\AddOns\\ElvUI_SLE\\media\\textures\\Warning-Small.tga')
			Slot.EnchantWarning:Point(Slot.Direction, Slot.ItemEnchant, Slot.Direction == 'LEFT' and 'RIGHT' or 'LEFT', Slot.Direction == 'LEFT' and 3 or -3, 0)
			Slot.EnchantWarning:SetScript('OnEnter', C.CommonScript.OnEnter)
			Slot.EnchantWarning:SetScript('OnLeave', C.CommonScript.OnLeave)

			-- Durability
			C.Toolkit.TextSetting(Slot, nil, { ['Tag'] = 'Durability', ['FontSize'] = 10, ['directionH'] = Slot.Direction, }, 'BOTTOM'..Slot.Direction, _G['Inspect'..slotName], 'BOTTOM'..(Slot.Direction == 'LEFT' and 'RIGHT' or 'LEFT'), Slot.Direction == 'LEFT' and 2 or -2, 3)

			-- Gem Socket
			for i = 1, MAX_NUM_SOCKETS do
				Slot['Socket'..i] = CreateFrame('Frame', nil, Slot)
				Slot['Socket'..i]:Size(E.db.sle.characterframeoptions.itemgem.socketSize)
				Slot['Socket'..i]:SetBackdrop({
					bgFile = E.media.blankTex,
					edgeFile = E.media.blankTex,
					tile = false, tileSize = 0, edgeSize = E.mult,
					insets = { left = 0, right = 0, top = 0, bottom = 0}
				})
				Slot['Socket'..i]:SetBackdropColor(0, 0, 0, 1)
				Slot['Socket'..i]:SetBackdropBorderColor(0, 0, 0)
				Slot['Socket'..i]:SetFrameLevel(InspectModelFrame:GetFrameLevel() + 1)

				Slot['Socket'..i].slotName = slotName
				Slot['Socket'..i].socketNumber = i

				Slot['Socket'..i].Socket = CreateFrame('Button', nil, Slot['Socket'..i])
				Slot['Socket'..i].Socket:SetBackdrop({
					bgFile = E.media.blankTex,
					edgeFile = E.media.blankTex,
					tile = false, tileSize = 0, edgeSize = E.mult,
					insets = { left = 0, right = 0, top = 0, bottom = 0}
				})
				Slot['Socket'..i].Socket:SetInside()
				Slot['Socket'..i].Socket:SetFrameLevel(Slot['Socket'..i]:GetFrameLevel() + 1)
				Slot['Socket'..i].Socket:RegisterForClicks('AnyUp')
				Slot['Socket'..i].Socket:SetScript('OnEnter', C.CommonScript.OnEnter)
				Slot['Socket'..i].Socket:SetScript('OnLeave', C.CommonScript.OnLeave)
				Slot['Socket'..i].Socket:SetScript('OnClick', GemSocket_OnClick)
				Slot['Socket'..i].Socket:SetScript('OnReceiveDrag', GemSocket_OnRecieveDrag)

				Slot['Socket'..i].Texture = Slot['Socket'..i].Socket:CreateTexture(nil, 'OVERLAY')
				Slot['Socket'..i].Texture:SetTexCoord(.1, .9, .1, .9)
				Slot['Socket'..i].Texture:SetInside()
			end

			Slot.Socket2:Point(Slot.Direction, Slot.Socket1, Slot.Direction == 'LEFT' and 'RIGHT' or 'LEFT', Slot.Direction == 'LEFT' and 1 or -1, 0)
			Slot.Socket3:Point(Slot.Direction, Slot.Socket2, Slot.Direction == 'LEFT' and 'RIGHT' or 'LEFT', Slot.Direction == 'LEFT' and 1 or -1, 0)

			Slot.SocketWarning = CreateFrame('Button', nil, Slot)
			Slot.SocketWarning:Size(E.db.sle.characterframeoptions.itemgem.warningSize)
			Slot.SocketWarning:RegisterForClicks('AnyUp')
			Slot.SocketWarning.Texture = Slot.SocketWarning:CreateTexture(nil, 'OVERLAY')
			Slot.SocketWarning.Texture:SetInside()
			Slot.SocketWarning.Texture:SetTexture('Interface\\AddOns\\ElvUI_SLE\\media\\textures\\Warning-Small.tga')
			Slot.SocketWarning:SetScript('OnEnter', C.CommonScript.OnEnter)
			Slot.SocketWarning:SetScript('OnLeave', C.CommonScript.OnLeave)
		end

		self[slotName] = Slot
	end

	-- GameTooltip for counting gem sockets and getting enchant effects
	self.ScanTTForEnchanting1 = CreateFrame('GameTooltip', 'KahootzArmoryScanTT_E1', nil, 'GameTooltipTemplate')
	self.ScanTTForEnchanting1:SetOwner(UIParent, 'ANCHOR_NONE')

	-- GameTooltip for checking that texture in tooltip is socket texture
	self.ScanTTForEnchanting2 = CreateFrame('GameTooltip', 'KahootzArmoryScanTT_E2', nil, 'GameTooltipTemplate')
	self.ScanTTForEnchanting2:SetOwner(UIParent, 'ANCHOR_NONE')

	-- For resizing paper doll frame when it toggled.
	self.ChangeCharacterFrameWidth = CreateFrame('Frame')
	self.ChangeCharacterFrameWidth:SetScript('OnShow', function() if InspectPaperDollFrame:IsVisible() then PANEL_DEFAULT_WIDTH = 448 IFO:ArmoryFrame_DataSetting() end end)
	self.ChangeCharacterFrameWidth:SetScript('OnHide', function() PANEL_DEFAULT_WIDTH = 338 end)

	CreateArmoryFrame = nil
end

function IFO:ChangeGradiantVisibility()
	for _, slotName in pairs(C.GearList) do
		if E.db.sle.characterframeoptions.shownormalgradient ~= false then
			f[slotName].Gradation:Show()
		else
			f[slotName].Gradation:Hide()
		end
	end
end

function IFO:ResizeErrorIcon()
	for _, slotName in pairs(C.GearList) do
		if slotName ~= 'ShirtSlot' and slotName ~= 'TabardSlot' then
			f[slotName].SocketWarning:Size(E.db.sle.characterframeoptions.itemgem.warningSize)
			f[slotName].EnchantWarning:Size(E.db.sle.characterframeoptions.itemenchant.warningSize)
			for i = 1, MAX_NUM_SOCKETS do
				f[slotName]['Socket'..i]:Size(E.db.sle.characterframeoptions.itemgem.socketSize)
			end
		end
	end
end

function IFO:ArmoryFrame_DataSetting()
	if not f:IsVisible() then return end

	-- Get Player Profession
	local Prof1, Prof2 = GetProfessions()
	local Prof1_Level, Prof2_Level = 0, 0
	IFO.PlayerProfession = {}

	if Prof1 then Prof1, _, Prof1_Level = GetProfessionInfo(Prof1) end
	if Prof2 then Prof2, _, Prof2_Level = GetProfessionInfo(Prof2) end
	if Prof1 and C.ProfessionList[Prof1] then IFO.PlayerProfession[(C.ProfessionList[Prof1])] = Prof1_Level end
	if Prof2 and C.ProfessionList[Prof2] then IFO.PlayerProfession[(C.ProfessionList[Prof2])] = Prof2_Level end

	local ErrorDetected
	local r, g, b
	local Slot, ItemLink, ItemRarity, BasicItemLevel, TrueItemLevel, ItemUpgradeID, ItemTexture, IsEnchanted, UsableEffect, CurrentLineText, GemID, GemTotal_1, GemTotal_2, GemCount, CurrentDurability, MaxDurability
	local arg1, itemID, enchantID, _, _, _, _, arg2, arg3, arg4, arg5, arg6

	for _, slotName in pairs(C.GearList) do
		if not (slotName == 'ShirtSlot' or slotName == 'TabardSlot') then
			Slot = f[slotName]

			do --<< Clear Setting >>--
				ErrorDetected, TrueItemLevel, IsEnchanted, UsableEffect, ItemRarity, ItemUpgradeID, ItemTexture = nil, nil, nil, nil, nil, nil, nil

				Slot.ItemLevel:SetText(nil)
				Slot.ItemEnchant:SetText(nil)
				Slot.Durability:SetText('')
				for i = 1, MAX_NUM_SOCKETS do
					Slot['Socket'..i].Texture:SetTexture(nil)
					Slot['Socket'..i].Socket.Link = nil
					Slot['Socket'..i].GemItemID = nil
					Slot['Socket'..i].GemType = nil
					Slot['Socket'..i]:Hide()
				end

				Slot.Socket1:Point('BOTTOM'..Slot.Direction, _G['Inspect'..slotName], 'BOTTOM'..(Slot.Direction == 'LEFT' and 'RIGHT' or 'LEFT'), Slot.Direction == 'LEFT' and 3 or -3, 0)
				Slot.EnchantWarning:Hide()
				Slot.EnchantWarning.Message = nil
				Slot.SocketWarning:Point(Slot.Direction, Slot.Socket1)
				Slot.SocketWarning:Hide()
				Slot.SocketWarning.Link = nil
				Slot.SocketWarning.Message = nil

				f.ScanTTForEnchanting1:ClearLines()
				f.ScanTTForEnchanting2:ClearLines()
				for i = 1, 10 do
					_G['KahootzArmoryScanTT_E1Texture'..i]:SetTexture(nil)
					_G['KahootzArmoryScanTT_E2Texture'..i]:SetTexture(nil)
				end
			end
			ItemLink = GetInventoryItemLink(InspectFrame.unit, Slot.ID)

			if ItemLink then
				do --<< Gem Parts >>--
					arg1, itemID, enchantID, _, _, _, _, arg2, arg3, arg4, arg5, arg6 = strsplit(':', ItemLink)

					f.ScanTTForEnchanting1:SetInventoryItem(InspectFrame.unit, Slot.ID)
					f.ScanTTForEnchanting2:SetHyperlink(format('%s:%s:%d:0:0:0:0:%s:%s:%s:%s:%s', arg1, itemID, enchantID, arg2, arg3, arg4, arg5, arg6))

					GemTotal_1, GemTotal_2, GemCount = 0, 0, 0

					-- First, Counting default gem sockets
					for i = 1, MAX_NUM_SOCKETS do
						ItemTexture = _G['KahootzArmoryScanTT_E2Texture'..i]:GetTexture()

						if ItemTexture and ItemTexture:find('Interface\\ItemSocketingFrame\\') then
							GemTotal_1 = GemTotal_1 + 1
							Slot['Socket'..GemTotal_1].GemType = strupper(gsub(ItemTexture, 'Interface\\ItemSocketingFrame\\UI--EmptySocket--', ''))
						end
					end

					-- Second, Check if slot's item enable to adding a socket
					if (slotName == 'WaistSlot' and UnitLevel(InspectFrame.unit) >= 70) then

						GemTotal_1 = GemTotal_1 + 1
						Slot['Socket'..GemTotal_1].GemType = 'PRISMATIC'
					end

					-- Apply current item's gem setting
					for i = 1, MAX_NUM_SOCKETS do
						ItemTexture = _G['KahootzArmoryScanTT_E1Texture'..i]:GetTexture()
						GemID = select(i, GetInventoryItemGems(Slot.ID))
						
						if Slot['Socket'..i].GemType and C.GemColor[Slot['Socket'..i].GemType] then
							r, g, b = unpack(C.GemColor[Slot['Socket'..i].GemType])
							Slot['Socket'..i].Socket:SetBackdropColor(r, g, b, .5)
							Slot['Socket'..i].Socket:SetBackdropBorderColor(r, g, b)
						else
							Slot['Socket'..i].Socket:SetBackdropColor(1, 1, 1, .5)
							Slot['Socket'..i].Socket:SetBackdropBorderColor(1, 1, 1)
						end

						if ItemTexture then
							Slot['Socket'..i]:Show()
							GemTotal_2 = GemTotal_2 + 1
							Slot.SocketWarning:Point(Slot.Direction, Slot['Socket'..i], (Slot.Direction == 'LEFT' and 'RIGHT' or 'LEFT'), Slot.Direction == 'LEFT' and 3 or -3, 0)

							if GemID then
								GemCount = GemCount + 1
								Slot['Socket'..i].Texture:SetTexture(ItemTexture)
								Slot['Socket'..i].GemItemID = GemID
								Slot['Socket'..i].Socket.Link = select(2, GetItemInfo(GemID))
							end
						end
					end
				end

				_, _, ItemRarity, BasicItemLevel, _, _, _, _, _, ItemTexture = GetItemInfo(ItemLink)
				r, g, b = GetItemQualityColor(ItemRarity)

				ItemUpgradeID = ItemLink:match(':(%d+)\124h%[')

				for i = 1, f.ScanTTForEnchanting1:NumLines() do
					CurrentLineText = _G['KahootzArmoryScanTT_E1TextLeft'..i]:GetText()

					if CurrentLineText:find(C.ItemLevelKey_Alt) then
						TrueItemLevel = tonumber(CurrentLineText:match(C.ItemLevelKey_Alt))
					elseif CurrentLineText:find(C.ItemLevelKey) then
						TrueItemLevel = tonumber(CurrentLineText:match(C.ItemLevelKey))
					elseif CurrentLineText:find(C.EnchantKey) then
						CurrentLineText = CurrentLineText:match(C.EnchantKey) -- Get enchant string
						CurrentLineText = gsub(CurrentLineText, ITEM_MOD_AGILITY_SHORT, AGI)
						CurrentLineText = gsub(CurrentLineText, ITEM_MOD_SPIRIT_SHORT, SPI)
						CurrentLineText = gsub(CurrentLineText, ITEM_MOD_STAMINA_SHORT, STA)
						CurrentLineText = gsub(CurrentLineText, ITEM_MOD_STRENGTH_SHORT, STR)
						CurrentLineText = gsub(CurrentLineText, ITEM_MOD_INTELLECT_SHORT, INT) --Intellect is to long for darth
						CurrentLineText = gsub(CurrentLineText, ITEM_MOD_CRIT_RATING_SHORT, CRIT_ABBR) -- Critical is too long
						--God damn russian localization team!
						CurrentLineText = gsub(CurrentLineText, "? ?????????? ?????????", ITEM_MOD_DODGE_RATING_SHORT)
						CurrentLineText = gsub(CurrentLineText, "? ?????????? ????????", ITEM_MOD_HASTE_RATING_SHORT)
						CurrentLineText = gsub(CurrentLineText, "? ?????????? ???????????", ITEM_MOD_PARRY_RATING_SHORT)
						CurrentLineText = gsub(CurrentLineText, "? ?????????? ??????????", ITEM_MOD_MASTERY_RATING_SHORT)
						CurrentLineText = gsub(CurrentLineText, ' + ', '+') -- Remove space
						CurrentLineText = gsub(CurrentLineText, "????????? ?????????? ???????? ????", "+? ???????? ????")

						if E.db.sle.characterframeoptions.itemenchant.show then
							Slot.ItemEnchant:Show()
							Slot.ItemEnchant:FontTemplate(LSM:Fetch("font", E.db.sle.characterframeoptions.itemenchant.font), E.db.sle.characterframeoptions.itemenchant.fontSize, E.db.sle.characterframeoptions.itemenchant.fontOutline)
							Slot.ItemEnchant:SetText('|cffceff00'..CurrentLineText)
						else
							Slot.ItemEnchant:Hide()
						end

						IsEnchanted = true
					elseif CurrentLineText:find(ITEM_SPELL_TRIGGER_ONUSE) then
						UsableEffect = true
					end
				end

				--<< ItemLevel Parts >>--
				if BasicItemLevel then
					if ItemUpgradeID then
						if ItemUpgradeID == '0' then
							ItemUpgradeID = nil
						else
							if not C.ItemUpgrade[ItemUpgradeID] then
								print('New Upgrade ID |cffceff00['..ItemUpgradeID..']|r : |cffceff00'..(TrueItemLevel - BasicItemLevel))
							end

							ItemUpgradeID = TrueItemLevel - BasicItemLevel
						end
					end
					Slot.ItemLevel:FontTemplate(LSM:Fetch("font", E.db.sle.characterframeoptions.itemlevel.font), E.db.sle.characterframeoptions.itemlevel.fontSize, E.db.sle.characterframeoptions.itemlevel.fontOutline)
					Slot.ItemLevel:SetText((Slot.Direction == 'LEFT' and TrueItemLevel or '')..(ItemUpgradeID and (Slot.Direction == 'LEFT' and ' ' or '')..(C.UpgradeColor[ItemUpgradeID] or '|cffaaaaaa')..'(+'..ItemUpgradeID..')|r'..(Slot.Direction == 'RIGHT' and ' ' or '') or '')..(Slot.Direction == 'RIGHT' and TrueItemLevel or ''))
				end

				-- Check Error
				if (not IsEnchanted and C.EnchantableSlots[slotName]) then
					ErrorDetected = true
					if E.db.sle.characterframeoptions.itemenchant.showwarning ~= false then
						Slot.EnchantWarning:Show()
						Slot.ItemEnchant:FontTemplate(LSM:Fetch("font", E.db.sle.characterframeoptions.itemenchant.font), E.db.sle.characterframeoptions.itemenchant.fontSize, E.db.sle.characterframeoptions.itemenchant.fontOutline)
						Slot.ItemEnchant:SetText('|cffff0000'..L['Not Enchanted'])
					end
				end

				if GemTotal_1 > GemTotal_2 or GemTotal_1 > GemCount then
					ErrorDetected = true

					if E.db.sle.characterframeoptions.itemgem.showwarning ~= false then
						Slot.SocketWarning:Show()
					end

					if GemTotal_1 > GemTotal_2 then
						if slotName == 'WaistSlot' then
							if TrueItemLevel < 300 then
								_, Slot.SocketWarning.Link = GetItemInfo(41611)	
							elseif TrueItemLevel < 417 then
								_, Slot.SocketWarning.Link = GetItemInfo(55054)
							else
								_, Slot.SocketWarning.Link = GetItemInfo(90046)
							end
						elseif slotName == 'HandsSlot' then
							Slot.SocketWarning.Link = GetSpellLink(114112)
						elseif slotName == 'WristSlot' then
							Slot.SocketWarning.Link = GetSpellLink(113263)
						end

							if slotName == 'WaistSlot' then
							Slot.SocketWarning.Message = L['Missing Buckle']
						elseif slotName == 'WristSlot' or slotName == 'HandsSlot' then
							Slot.SocketWarning.Message = '|cff71d5ff'..GetSpellInfo(110396)..'|r : '..L['Missing Socket']
						end
					else
						Slot.SocketWarning.Message = '|cffff5678'..(GemTotal_1 - GemCount)..'|r '..L['Empty Socket']
					end

					if GemTotal_1 ~= GemTotal_2 and slotName == 'WaistSlot' then
						Slot.SocketWarning:SetScript('OnClick', function(self, button)
							local itemName, itemLink

							if TrueItemLevel < 300 then
								itemName, itemLink = GetItemInfo(41611)
							elseif TrueItemLevel < 417 then
								itemName, itemLink = GetItemInfo(55054)
							else
								itemName, itemLink = GetItemInfo(90046)
							end
		
							if HandleModifiedItemClick(itemLink) then
							elseif IsShiftKeyDown() then
								if button == 'RightButton' then
									SocketInventoryItem(Slot.ID)
								elseif BrowseName and BrowseName:IsVisible() then
									AuctionFrameBrowse_Reset(BrowseResetButton)
									BrowseName:SetText(itemName)
									BrowseName:SetFocus()
								end
							end
						end)
					end
				end
			end

			-- Change Gradation
			if ErrorDetected and E.db.sle.characterframeoptions.showerrorgradient ~= false then
				if Slot.Direction == 'LEFT' then
					Slot.Gradation:SetTexCoord(0, .5, .5, 1)
				else
					Slot.Gradation:SetTexCoord(.5, 1, .5, 1)
				end
			else
				if Slot.Direction == 'LEFT' then
					Slot.Gradation:SetTexCoord(0, .5, 0, .5)
				else
					Slot.Gradation:SetTexCoord(.5, 1, 0, .5)
				end
			end
		end
	end
	
	if E.db.sle.characterframeoptions.showimage ~= false then
		f.BG:SetTexture('Interface\\AddOns\\ElvUI_SLE\\media\\textures\\Space.tga')
	else
		f.BG:SetTexture(nil);
	end
	f.AverageItemLevel:SetText(C.Toolkit.Color_Value(L['Average'])..' : '..format('%.2f', IFO:GetItemLvL(InspectFrame.unit)))
end

function IFO:StartArmoryFrame()
	-- Setting frame
	InspectFrame:SetHeight(444)
	InspectFrame:SetWidth(444)
	--InspectFrameInsetRight:SetPoint('TOPLEFT', InspectFrameInset, 'TOPRIGHT', 110, 0)
	--CharacterFrameExpandButton:SetPoint('BOTTOMRIGHT', CharacterFrameInsetRight, 'BOTTOMLEFT', 0, 1)

	-- Move right equipment slots
	InspectHandsSlot:SetPoint('TOPRIGHT', InspectPaperDollItemsFrame, 'TOPRIGHT', -7, -62)

	-- Move bottom equipment slots
	InspectMainHandSlot:SetPoint('BOTTOMLEFT', InspectPaperDollItemsFrame, 'BOTTOMLEFT', 181, 14)

	-- Model Frame
	InspectModelFrame:Size(341, 302)
	InspectModelFrame:SetPoint('TOPLEFT', InspectPaperDollFrame, 'TOPLEFT', 52, -90)
	InspectModelFrame.BackgroundTopLeft:Hide()
	InspectModelFrame.BackgroundTopRight:Hide()
	InspectModelFrame.BackgroundBotLeft:Hide()
	InspectModelFrame.BackgroundBotRight:Hide()
	
	-- Character Control Frame
	InspectModelFrameControlFrame:ClearAllPoints()
	InspectModelFrameControlFrame:SetPoint('BOTTOM', InspectModelFrame, 'BOTTOM', 0, 0)

	if CreateArmoryFrame then
		CreateArmoryFrame(KahootzArmory)
	end
	IFO:ArmoryFrame_DataSetting()
	-- Run ArmoryMode
	--IFO:RegisterEvent('SOCKET_INFO_SUCCESS', 'ArmoryFrame_DataSetting')
	--IFO:RegisterEvent('PLAYER_EQUIPMENT_CHANGED', 'ArmoryFrame_DataSetting')
	--IFO:RegisterEvent('PLAYER_ENTERING_WORLD', 'ArmoryFrame_DataSetting')
	--IFO:RegisterEvent('UNIT_INVENTORY_CHANGED', 'ArmoryFrame_DataSetting')
	--IFO:RegisterEvent('EQUIPMENT_SWAP_FINISHED', 'ArmoryFrame_DataSetting')
	--IFO:RegisterEvent('UPDATE_INVENTORY_DURABILITY', 'ArmoryFrame_DataSetting')
	--IFO:RegisterEvent('ITEM_UPGRADE_MASTER_UPDATE', 'ArmoryFrame_DataSetting')

	-- For frame resizing
	--f.ChangeCharacterFrameWidth:SetParent(PaperDollFrame)
	--if PaperDollFrame:IsVisible() then
		--f.ChangeCharacterFrameWidth:Show()
		--CharacterFrame:SetWidth(CharacterFrameInsetRight:IsShown() and 650 or 448)
	--end
end

function IFO:INSPECT_READY(GUID)
	if InspectFrame then
	if InspectFrame['unit'] then
		if UnitGUID(InspectFrame['unit']) == GUID then
			IFO:ScheduleTimer("TimerFeedback", .05)
		end
	end
	end
end

function IFO:TimerFeedback()
	InspectPaperDollItemSlotButton_Update(InspectHeadSlot);
    InspectPaperDollItemSlotButton_Update(InspectNeckSlot);
    InspectPaperDollItemSlotButton_Update(InspectShoulderSlot);
    InspectPaperDollItemSlotButton_Update(InspectBackSlot);
    InspectPaperDollItemSlotButton_Update(InspectChestSlot);
    InspectPaperDollItemSlotButton_Update(InspectShirtSlot);
    InspectPaperDollItemSlotButton_Update(InspectTabardSlot);
    InspectPaperDollItemSlotButton_Update(InspectWristSlot);
    InspectPaperDollItemSlotButton_Update(InspectHandsSlot);
    InspectPaperDollItemSlotButton_Update(InspectWaistSlot);
    InspectPaperDollItemSlotButton_Update(InspectLegsSlot);
    InspectPaperDollItemSlotButton_Update(InspectFeetSlot);
    InspectPaperDollItemSlotButton_Update(InspectFinger0Slot);
    InspectPaperDollItemSlotButton_Update(InspectFinger1Slot);
    InspectPaperDollItemSlotButton_Update(InspectTrinket0Slot);
    InspectPaperDollItemSlotButton_Update(InspectTrinket1Slot);
    InspectPaperDollItemSlotButton_Update(InspectMainHandSlot);
    InspectPaperDollItemSlotButton_Update(InspectSecondaryHandSlot);
end


function IFO:AfterLoad()
	if not E.private.sle.characterframeoptions.enable then return end
	hooksecurefunc('InspectPaperDollFrame_SetLevel', function()
		f:SetParent(InspectPaperDollFrame)
		if (not InspectFrame.unit) then
			return;
		end
		local unit, level = InspectFrame.unit, UnitLevel(InspectFrame.unit);
		local specID = GetInspectSpecialization(InspectFrame.unit);
		local classDisplayName, class = UnitClass(InspectFrame.unit); 
		local classColorString = RAID_CLASS_COLORS[class].colorStr;
		local specName, _;
		local PLAYER_LEVEL = "|c%s%s %s %s %s|r"
		local PLAYER_LEVEL_NO_SPEC = "|c%s%s %s %s|r"
		if (specID) then
			_, specName = GetSpecializationInfoByID(specID);
		end
		 
		if ( level == -1 ) then
			level = "??";
		end
		 
		if (specName and specName ~= "") then
			InspectLevelText:SetFormattedText(PLAYER_LEVEL, classColorString, LEVEL, level, specName, classDisplayName);
		else
			InspectLevelText:SetFormattedText(PLAYER_LEVEL_NO_SPEC, classColorString, LEVEL, level, classDisplayName);
		end
		
		InspectFrameTitleText:ClearAllPoints()
		InspectFrameTitleText:Point('TOP', InspectModelFrame, 'TOP', 0, 50)
		InspectFrameTitleText:SetParent(f)
		InspectLevelText:ClearAllPoints()
		InspectLevelText:SetPoint('TOP', InspectFrameTitleText, 'BOTTOM', 0, 0)
		InspectLevelText:SetParent(f)
		IFO:ScheduleTimer('StartArmoryFrame',.1)
	end)

		
		
end

E:RegisterModule(IFO:GetName())